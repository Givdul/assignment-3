//Global variables
let balance = 0;
let pay = 0;
let loan = 0;
let laptopJson = {};
//API functions
//Get the API, store it in a local variable and update elements which requires info from the api
fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        let counter = myJson.length;
        laptopJson = myJson;

        for (let i = 0; i < myJson.length; i++){
            document.getElementById("laptops").innerHTML += "<option value='" + i + "'>" + myJson[i].title + "</option>";
        }
        specs(0);
    });
//Print features of selected computer
function specs(id) {
        document.getElementById("featureList").innerHTML = "";
        for (let i = 0; i < laptopJson[id].specs.length; i++) {
            document.getElementById("featureList").innerHTML += "<p>" + laptopJson[id].specs[i] + "</p>";
        }

        document.getElementById("laptopName").innerHTML = laptopJson[id].title;
        document.getElementById("laptopDescription").innerHTML = laptopJson[id].description;
        document.getElementById("laptopPrice").innerHTML = laptopJson[id].price + " NOK";
        document.getElementById("laptopImg").setAttribute("src", "https://noroff-komputer-store-api.herokuapp.com/" + laptopJson[id].image);
}
//other functions
//Do work get money (pay plus 100)
function work(){
    pay += 100;
    document.getElementById("payValue").innerHTML = pay;
}
//Puts pay amount in the bank, if a loan is not paid of 10% is taken
function bank(){
    if (loan != 0){
        loan -= pay*0.10;
        pay = pay*0.90;
    }

    balance += pay;
    pay = 0;
    document.getElementById("payValue").innerHTML = pay;
    document.getElementById("balanceValue").innerHTML = balance;
    document.getElementById("loanValueSpan").innerHTML = loan;
}
//Loan me money, cant loan more than double of balance
function loanMe() {
    if (loan != 0) {
        alert("You already have an outstanding loan!\nYou cannot have two loans at the same time.")
    }
    else {
        let loanValue = prompt("How much kr would you like to loan?", "0");
        if (loanValue != null) {
            tempPrompt = Number(loanValue)
            if (tempPrompt > (balance * 2)) {
                alert("You are not eligible for this loan amount.")
            }
            else {
                loan = tempPrompt;
                balance += loan;
                document.getElementById("repayButton").style.visibility = "visible";
                document.getElementById("loanValue").style.visibility = "visible";
                document.getElementById("balanceValue").innerHTML = balance;
                document.getElementById("loanValueSpan").innerHTML = loan;
            }
        }
    }
}
//Repays the loan with the current pay amount
function repay(){
    loan = (loan - pay);
    if(loan < 0){//Put money in balance if you overpay the loan
        loan =Math.abs(loan);//Changes negative number to positive
        document.getElementById("balanceValue").innerHTML = (balance += loan);
        loan = 0;
    }
    if (loan <= 0){
        document.getElementById("repayButton").style.visibility = "hidden";
        document.getElementById("loanValue").style.visibility = "hidden";
    }
    pay = 0;
    document.getElementById("payValue").innerHTML = pay;
    document.getElementById("loanValueSpan").innerHTML = loan;
}


function buyLaptop(){
        let laptopCost = document.getElementById("laptopPrice").innerHTML;
        laptopCost = parseInt(laptopCost);
        if (balance < laptopCost){
            alert("You cannot afford this laptop!");
        }
        else {
            balance -= laptopCost;
            document.getElementById("balanceValue").innerHTML = balance;
            alert("Congratulations, you are the owner of " + document.getElementById("laptopName").innerHTML + "!");
        }
}