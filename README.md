# Assignment 4 - Komputer store

Javascript assignment from the Noroff acellerate course.


## Run Locally

* Clone the project

* Open Index.html

* Alternatively open in VScode and run with LiveServer extension

* press buttons


## Authors

Ludvig Hansen


## License

[MIT](https://choosealicense.com/licenses/mit/)
